export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user/login', name: 'login', component: './User/Login', },
      { component: '404', },
    ],
  },

  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      { path: '/', redirect: '/Submissions', },
      // { path: '/Department', component: './Department', name: 'Submissions', authority: ['manager','teacher'], }, 
      { path: '/Submissions', component: './Submissions', name: 'Submissions', authority: ['manager'] },
      {
        path: '/List', component: './List', name: 'List 1233', authority: ['manager']
      },
      { path: '/Submissions/Details', component: './Submissions/SubDetails', name: 'Submissions', authority: ['manager'],hideInMenu: true, },
      // { path: '/Staff', component: './Staff', name: 'Nhân Viên', authority: ['manager'], }, 
      // { path: '/BankCard', component: './BankCard', name: 'Thẻ Ngân Hàng', authority: ['manager'], }, 
      // { path: '/Service', component: './Service', name: 'Dịch vụ', authority: ['manager'], }, 
      // { path: '/Profile', component: './Profile', name: 'Account Info', authority: ['manager'], }, 
      // { path: '/Courses', component: './Courses', name: 'List Courses', authority: ['manager'], },
      { component: '404', },
    ],
  },
];
