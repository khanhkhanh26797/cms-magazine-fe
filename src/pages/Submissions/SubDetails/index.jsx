import { PlusOutlined } from '@ant-design/icons';
import { Button, Table, Card, Input, Drawer, PageHeader, Descriptions,Popconfirm } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import React, { useState, useEffect } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import { getAllContributions } from '@/services/submiss';
import moment from 'moment';
const DocIframe = ({ source }) => {
  if (!source) {
    return <div>Loading...</div>;
  }
  // const src ="http://localhost:1337/uploads/974877a4db46498585da641a22ad7769.docx"
  const src = encodeURIComponent(source);
  return (
    <div>
      <iframe
        src={"https://docs.google.com/viewer?url=" + src + "&embedded=true"}
        title="file"
        width="80%"
        height="600"
      ></iframe>
    </div>
  );
};

const TableList = (props) => {
  console.log(props)
  const {contribution} = props.location.state
  const [data, setData] = useState([]);
  const [loading, setLoaing] = useState(false);
  console.log(contribution)
  const fetchData = async () => {
    setLoaing(true);
    const result = await getAllContributions();
    result.map(item => {
      item.stdFaculty = item.faculty.name;
      item.stdName = item.student.name;
      item.createdTime = moment(item.createdAt).format('DD/MM/YYYY');
    });
    console.log(result);
    setLoaing(false);
    setData(result);
  };
  
  useEffect(() => {
    // fetchData();
  }, []);
  return (
    <>
      <div className="site-page-header-ghost-wrapper">
        <PageHeader
          ghost={false}
          title={`${contribution.titile}`}
          extra={
            [
              // <Button key="2" loading={loading} size="small" type="primary" onClick={() => fetchData()}>Thêm nhân viên</Button>,
              // <Button key="2" loading={loading} size="small" type="primary" onClick={() => fetchData()}>Reload</Button>,
            ]
          }
        ></PageHeader>
      </div>
      <DocIframe source="https://file-examples-com.github.io/uploads/2017/02/file-sample_100kB.doc" />
      
      {/* <Table dataSource={data} columns={columns} loading={loading}  /> */}

    </>
  );
};

export default TableList;
