import { PlusOutlined } from '@ant-design/icons';
import { Button, Table, Card, Input, Drawer, PageHeader, Descriptions,Popconfirm } from 'antd';
import { Link } from 'umi';
import React, { useState, useEffect } from 'react';
import { PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import { queryRule, updateRule, addRule, removeRule } from './service';
import { getAllContributions } from '@/services/submiss';
import moment from 'moment';
import { DownloadOutlined } from '@ant-design/icons';
import {prefixUrl} from '../../utils/request'
const url = prefixUrl.product
const columns = [
  {
    title: 'Student Name',
    dataIndex: 'stdName',
    key: 'idName',
  },
  {
    title: 'Student Email',
    dataIndex: 'stdEmail',
    key: 'idEmail',
  },
  {
    title: 'Student Faculty',
    dataIndex: 'stdFaculty',
    key: 'idFaculty',
    filters: [
      {
        text: "Information Technology",
        value: "Information Technology"
      },
      {
        text: "Business",
        value: "Business"
      },
      {
        text: "Graphic Design",
        value: "Graphic Design"
      }
    ],
    onFilter: (value, record) => record.stdFaculty.indexOf(value) === 0
  },
  {
    title: 'Submission Title',
    dataIndex: 'titile',
    key: 'idTitile',
  },
  {
    title: 'Sumbmit Day',
    dataIndex: 'createdTime',
    key: 'idDay',
    sorter: (a, b) => moment(a.createdAt).valueOf() - moment(b.createdAt).valueOf()
  },
  {
    title: 'View Details',
    // dataIndex: 'titile',
    key: 'idAction',
    render: (text, record) =>
      <Button >
        <Link to={{ pathname: '/Submissions/Details', state: { contribution: record} }}>Go Details</Link>
      </Button>
  },
  {
    title: 'Download',
    // dataIndex: 'titile',
    key: 'idDownload',
    render: (text, record) =>
      <Button type="primary" icon={<DownloadOutlined />} size='small' onClick={()=> onDownload(record)}>
      </Button>
      //  <Button type="primary" icon={<DownloadOutlined />} size='small' onClick={()=> onDownload(record)}>
      //  </Button>
  },
];
function onDownload(record) {
  Object.assign(document.createElement('a'), {
    target: '_blank',
    href: `${url}/${record.article[0].url} `,
  }).click();
}

const TableList = () => {
  const [data, setData] = useState([]);
  const [loading, setLoaing] = useState(false);

  const fetchData = async () => {
    setLoaing(true);
    const result = await getAllContributions();
    result.map(item => {
      item.stdFaculty = item.faculty.name;
      item.stdName = item.student.name;
      item.createdTime = moment(item.createdAt).format('DD/MM/YYYY');
    });
    console.log(result);
    setLoaing(false);
    setData(result);
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <div className="site-page-header-ghost-wrapper">
        <PageHeader
          ghost={false}
          title={`List Submissions`}
          extra={
            [
              // <Button key="2" loading={loading} size="small" type="primary" onClick={() => fetchData()}>Thêm nhân viên</Button>,
              // <Button key="2" loading={loading} size="small" type="primary" onClick={() => fetchData()}>Reload</Button>,
            ]
          }
        ></PageHeader>
      </div>
      <Table dataSource={data} columns={columns} loading={loading} />
    </>
  );
};

export default TableList;
