import styles from './index.less';
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, PageHeader, Modal, Select, Row, Col, Option } from 'antd';
import { getAllUsers, createUsers, getUsersDetails,editUsers,deleteUsers} from '@/services/Staff';
const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditForm = ({ index, ...props }) => {
  const { getFieldDecorator } = form;
  return (
    <Form
    // onFinish={this.props.onFinish}
    // form={this.state.dataModalEdit}
    >
      <Form.Item label="Họ và tên đệm" name="first_name">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Tên nhân viên" name="last_name">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Tên đăng nhập" name="login">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Email đăng nhập" name="email">
        <Input placeholder="" />
      </Form.Item>

      <Form.Item label="Số điện thoại" name="phone">
        <Input placeholder="" type="phone" />
      </Form.Item>
      <Form.Item
        name="authorities"
        label="Select"
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Chọn loại tài khoản trước khi tạo!',
          },
        ]}
      >
        <Select placeholder="Chọn loại tài khoản">
          <Select value="0">Quản lý</Select>
          <Select value="1">Role 2</Select>
          <Select value="2">Trưởng nhóm</Select>
          <Select value="3">Nhân viên</Select>
        </Select>
      </Form.Item>
      <Form.Item >
        <Button
          type="primary"
          htmlType="submit"
        >
          Thêm
              </Button>
        <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
          Huỷ
        </Button>,
          </Form.Item>
    </Form>
  );
};
function CustomTextInput(props) {
  return (
    <div>
      <input ref={props.inputRef} />
    </div>
  );
}

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async (e) => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
        <div
          className="editable-cell-value-wrap"
          style={{
            paddingRight: 24,
          }}
          onClick={toggleEdit}
        >
          {children}
        </div>
      );
  }

  return <td {...restProps}>{childNode}</td>;
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    // this.ref.textInput='11',
    this.columns = [
      {
        title: 'Họ tên',
        dataIndex: 'first_name',
        key: 'id',
      },
      {
        title: 'Tài khoản',
        dataIndex: 'first_name',
        key: 'id',
      },
      {
        title: 'Phòng ban',
        dataIndex: 'first_name',
        key: 'id',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Giới tính',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Số điện thoại',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Ngày cập nhập',
        dataIndex: 'email',
        key: 'id',
      },
      {
        title: 'Thay đổi',
        dataIndex: 'edit',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure?" onConfirm={() => this.handEdit(record.id)}>
              <a>Thay đổi</a>
            </Popconfirm>
          ) : null,
      },
      {
        title: 'Xoá',
        dataIndex: 'operation',
        render: (text, record) =>
          this.state.dataSource.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
              <a>Xoá</a>
            </Popconfirm>
          ) : null,
      },
    ];
    this.state = {
      form: [],
      dataSource: [],
      count: 2,
      modalVisible: false,
      modalVisibleEdit: false,
      formInput: null,
      first_name: null,

      // dataModalEdit: {
      //   authorities: [
      //     1
      //   ],
      //   department_id: 1,
      //   first_name: "khanh",
      //   last_name: "khanh",
      //   login: "khanh",
      //   email: "khanh",
      //   phone: "khanh",
      // },
      dataModalEdit: null
    };

  }

  componentDidMount = async () => {
    await this.getData()

    // this.textInput.current.focusTextInput();
  }

  addItem = () => {
    this.setState({ modalVisible: true })
  };
  closeModal = () => {
    this.setState({ modalVisible: false })
  };

  getData = async () => {
    const result = await getAllUsers();
    this.setState({ dataSource: result.data })
  }
  getUsersDetails = async (id) => {
    const result = await getUsersDetails(id);
    console.log(result)
    this.setState({ dataModalEdit: result.data })
    // this.setState({ first_name: result.data.first_name })

    // this.props.dataModalEdit.setFieldsValue({
    //   first_name: result.data.first_name,
    // })
  }

  createItem = async (values) => {
    console.log('Success:', values);
    let body = {
      authorities: [
        values.authorities
      ],
      department_id: 1,
      first_name: values.first_name,
      last_name: values.last_name,
      login: values.login,
      email: values.email,
      phone: values.phone,
    }

    const result = await createUsers(body);
    console.log(result)
    this.setState({ modalVisible: false })
  };
  editItem = async (values) => {
    console.log('Success:', values);
    let body = {
      authorities: [
        values.authorities
      ],
      department_id: 1,
      first_name: values.first_name,
      last_name: values.last_name,
      login: values.login,
      email: values.email,
      phone: values.phone,
    }
    const result = await editUsers(body,this.state.dataModalEdit.id);
    console.log(result)
    this.setState({ modalVisibleEdit: false })
  };
  handleDelete = async (id) => {
    console.log(id)
    const result = await deleteUsers(id);
    console.log(result)
    // const dataSource = [...this.state.dataSource];
    // this.setState({
    //   dataSource: dataSource.filter((item) => item.key !== key),
    // });
  };
  handEdit = async (id) => {
    console.log(id)
    await this.getUsersDetails(id)
    this.setState({ modalVisibleEdit: true })
    // const dataSource = [...this.state.dataSource];
    // this.setState({
    //   dataSource: dataSource.filter((item) => item.key !== key),
    // });
  };
  handleAdd = () => {
    const { count, dataSource } = this.state;
    const newData = {
      key: count,
      name: `Edward King ${count}`,
      age: 32,
      address: `London, Park Lane no. ${count}`,
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1,
    });
  };
  handleSave = (row) => {
    const newData = [...this.state.dataSource];
    const index = newData.findIndex((item) => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    this.setState({
      dataSource: newData,
    });
  };

  render() {

    console.log(this.state.dataModalEdit)
    const { dataSource } = this.state;
    const components = {
      body: {
        row: EditableRow,
        cell: EditableCell,
      },
    };
    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: (record) => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>

        <PageHeader
          ghost={false}
          title={`Danh sách nhân viên`}
          extra={[
            <Button
              onClick={() => this.addItem()}
              type="primary"
              style={{
                marginBottom: 16,
              }}
            >
              Thêm nhân viên
          </Button>,
            <Button
              onClick={() => { this.getData() }}
              type="primary"
              style={{
                marginBottom: 16,
              }}

            >
              Reload
            </Button>,
          ]}
        ></PageHeader>

        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
        <Form >
          {/* <Input placeholder='khanh' value={this.state.dataModalEdit && this.state.dataModalEdit.last_name} /> */}
          <Button
            type="primary"
            htmlType="submit"
          >
            Thêm
              </Button>
        </Form>

        <Modal
          destroyOnClose
          title="Thêm mới nhân viên"
          visible={this.state.modalVisible}
          onCancel={() => this.closeModal()}

          footer={[
            <Form
              onFinish={this.createItem}
            >
              <Form.Item label="Họ và tên đệm" name="first_name" >
                <Input placeholder="Họ và tên đệm" />
              </Form.Item>

              <Form.Item label="Tên nhân viên" name="last_name">
                <Input placeholder="Tên nhân viên" />
              </Form.Item>

              <Form.Item label="Tên đăng nhập" name="login">
                <Input placeholder="Tên đăng nhập" />
              </Form.Item>

              <Form.Item label="Email đăng nhập" name="email">
                <Input placeholder="" type="email" />
              </Form.Item>

              <Form.Item label="Số điện thoại" name="phone">
                <Input placeholder="" type="phone" />
              </Form.Item>

              <Form.Item
                name="authorities"
                label="Select"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Chọn loại tài khoản trước khi tạo!',
                  },
                ]}
              >
                <Select placeholder="Chọn loại tài khoản">
                  <Select value="0">Quản lý</Select>
                  <Select value="1">Role 2</Select>
                  <Select value="2">Trưởng nhóm</Select>
                  <Select value="3">Nhân viên</Select>
                </Select>
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.closeModal()}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>

          ]}
        >
        </Modal>,
        {/* <ModalStaff visible={this.state.modalVisibleEdit} data={this.state.modalVisibleEdit}></ModalStaff> */}
        <Modal
          destroyOnClose
          title="Chỉnh sử thông tin nhân viên"
          visible={this.state.modalVisibleEdit}
          onCancel={() => this.setState({ modalVisibleEdit: false })}
          footer={[

            <Form
              onFinish={this.editItem}
            // form = {this.prop.dataModalEdit}
            >
              {/* <Input placeholder="" value={this.state.dataModalEdit && this.state.dataModalEdit.last_name} /> */}
              
              <Form.Item label="Họ và tên đệm" name="first_name"  initialValue={this.state.dataModalEdit && this.state.dataModalEdit.first_name} >
                <Input/>
              </Form.Item>
              
              <Form.Item label="Tên nhân viên" name="last_name" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.last_name}>
                <Input/>
              </Form.Item>

              <Form.Item label="Tên đăng nhập" name="login" initialValue={this.state.dataModalEdit&&this.state.dataModalEdit.login}>
                <Input placeholder="" />
              </Form.Item>

              <Form.Item label="Email đăng nhập" name="email" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.email}>
                <Input placeholder="" type="email" />
              </Form.Item>

              <Form.Item label="Số điện thoại" name="phone" initialValue={this.state.dataModalEdit && this.state.dataModalEdit.email}>
                <Input placeholder="" type="phone" />
              </Form.Item>

              <Form.Item
                name="authorities"
                label="Select"
                hasFeedback
                // initialValue={this.state.dataModalEdit && this.state.dataModalEdit.authorities}
                rules={[
                  {
                    required: true,
                    message: 'Chọn loại tài khoản trước khi tạo!',
                  },
                ]}
              >
                <Select placeholder="Chọn loại tài khoản">
                  <Select value="0">Quản lý</Select>
                  <Select value="1">Role 2</Select>
                  <Select value="2">Trưởng nhóm</Select>
                  <Select value="3">Nhân viên</Select>
                </Select>
              </Form.Item>
              <Form.Item >
                <Button
                  type="primary"
                  htmlType="submit"
                >
                  Thêm
              </Button>
                <Button key="back" onClick={() => this.setState({ modalVisibleEdit: false })}>
                  Huỷ
        </Button>,
          </Form.Item>
            </Form>
          ]}
        >
        </Modal>
      </div>
    );
  }
}

export default () => (
  <div className={styles.container}>
    <div id="components-table-demo-edit-cell">
      <EditableTable />
    </div>
  </div>
);
