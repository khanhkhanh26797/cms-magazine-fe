/**
 * request 网络请求工具
 * 更详细的 api 文档: https://github.com/umijs/umi-request
 */
import { extend } from 'umi-request';
import { notification } from 'antd';
// import { getLocale, formatMessage } from 'umi-plugin-react/locale';
export const prefixUrl = {
  product: 'http://localhost:1337',
  // develop: 'http://138.197.46.187:1340/api',
  develop: 'http://localhost:1337',
};
const codeMessage = {
  200: 'OK',
  201: 'Created',
  202: 'Accepted',
  204: 'No Content',
  400: 'Bad Request',
  401: 'Unauthorized',
  403: 'Forbidden',
  404: 'Not Found',
  406: 'Not Acceptable',
  410: 'Gone',
  422: 'Unprocessable Entity',
  500: 'Internal Server Error',
  502: 'Bad Gateway',
  503: 'Service Unavailable',
  504: 'Gateway Timeout',
};
/**
 * 异常处理程序
 */

const errorHandler = error => {
  const { response, data } = error;
  console.log(response);
  const errorMessage = codeMessage[response.status] || formatMessage({ id: 'common.requestError' });
  let errorDescription = '';
  if (data.message && data.message.length) {
    errorDescription = data.message[0].messages.map(m => m.message).join(',');
  } else {
    errorDescription = data.error;
  }
  const { status } = response;
  if (status === 401) {
    notification.error({
      key: errorMessage,
      message: errorMessage,
      description: formatMessage({ id: 'authen.unauthorized' }),
    });
    // @HACK
    window.g_app._store.dispatch({
      type: 'login/logout',
    });
    return;
  }
  if (status === 429) {
    notification.error({
      message: 'You apptempt to login to much times, wait 30 seconds to login again !',
    });
    // @HACK
    window.g_app._store.dispatch({
      type: 'login/logout',
    });
    return;
  }
  notification.error({
    key: errorMessage,
    message: errorMessage,
    description: errorDescription,
  });
  // environment should not be used
  if ((status >= 400 && status < 422) || (status <= 504 && status >= 500)) {
    if (status === 502) {
      // @HACK
      window.g_app._store.dispatch({
        type: 'login/exception500',
      });
      return;
    }
    throw Error(error);
  }
};
/**
 * 配置request请求时的默认参数
 */

const request = extend({
  // errorHandler, // Default error handling
  prefix: process.env.NODE_ENV === 'production' ? prefixUrl.product : prefixUrl.develop,
  // credentials: 'include', // Whether the default request comes with a cookie
  headers: {
    // 'Accept-Language': getLocale(),
    'Content-Type':'application/json'
  },
});

request.interceptors.request.use((url, options) => {
  const token = localStorage.getItem('token') || null;
  const role = localStorage.getItem('bms-token');

  if (token && role ) {
    options.headers = {
      ...options.headers,
      Authorization: `Bearer ${token}`,
    };
  }
  return {
    url: `${url}`,
    options: {
      ...options,
      interceptors: true,
    },
  };
});
export default request;
