import { stringify } from 'querystring';
import { history } from 'umi';
import { fakeAccountLogin,loginWithLocal } from '@/services/login';
import { setAuthority ,setToken} from '@/utils/authority';
import { reloadAuthorized } from '@/utils/Authorized';
import { getPageQuery } from '@/utils/utils';
const Model = {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    *login({ payload }, { call, put }) {
      try {
        const response = yield call(loginWithLocal, payload);
        if (response && response.user) {
          yield put({
            type: 'changeLoginStatus',
            payload: {
              currentAuthority: response.user && response.user.role && response.user.role.type,
              token: response.jwt,
              status: 'loggedIn',
            },
          });
          yield put(
            routerRedux.replace({
              pathname: '/',
              search: stringify({
                redirect: window.location.href,
              }),
            })
          );
          reloadAuthorized();
        }
      } catch (error) {
        /* eslint-disable-next-line no-console */
        console.log(error);
      }
    },


    logout() {
      const { redirect } = getPageQuery(); // Note: There may be security issues, please note

      if (window.location.pathname !== '/user/login' && !redirect) {
        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.currentAuthority);
      setToken(payload.token);
      return { ...state, status: payload.status, type: payload.type };
    },
  },
};
export default Model;
