import { getAllDepartment } from '@/services/department';
const DepartmentModel = {
  namespace: 'department',
  state: {
    // currentUser: {},
    allDepartment:{},
  },
  effects: { 

    *fetchGetAllDepartment(_, { call, put }) {
      const response = yield call(getAllDepartment);
      yield put({
        type: 'saveAllDepartment',
        payload: response,
      });
    },
  },
  reducers: {
    saveAllDepartment(state, action) {
      return { ...state, allDepartment: action.payload || {} };
    },

  },
};
export default DepartmentModel;
