import request from '@/utils/request';
import { stringify } from 'qs';
export async function getAllContributions() {
  return request('/contributions', {
    method: 'GET',
  });
}
export async function getBankCardDetails(id) {
  return request(`/bank-cards/${stringify(id)}`, {
      method: 'GET',
    });
  } 

  export async function createBankCard(params) {
    return request('/bank-cards', {
      method: 'POST',
      data: params,
    });
  } 
  export async function editBankCard(params,id) {
    return request(`/bank-cards/${stringify(id)}`, {
      method: 'PUT',
      data: params,
    });
  }

