import request from '@/utils/request';
import { stringify } from 'qs';
export async function getAllDepartment() {
  return request('/departments', {
    method: 'GET',
  });
}
export async function getDepartmentDetails(id) {
  return request(`/departments/${stringify(id)}`, {
      method: 'GET',
    });
  } 

  export async function createDepartment(params) {
    return request('/departments', {
      method: 'POST',
      data: params,
    });
  } 
  export async function editDepartment(params,id) {
    return request(`/departments/${stringify(id)}`, {
      method: 'PUT',
      data: params,
    });
  }
  // export async function deleteDepartment(params) {
  //   return request('/departments', {
  //     method: 'DELETE',
  //     data: params,
  //   });
  // }
