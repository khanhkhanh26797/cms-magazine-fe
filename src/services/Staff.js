import request from '@/utils/request';
import { stringify } from 'qs';
export async function getAllUsers() {
  return request('/users', {
    method: 'GET',
  });
}
export async function getUsersDetails(id) {
  return request(`/users/${id}`, {
      method: 'GET',
    });
  } 

  export async function createUsers(data) {
    return request('/users', {
      method: 'POST',
      data
    });
  }

  export async function editUsers(data,id) {
    return request(`/users/${id}`, {
      method: 'PUT',
      data
    });
  }
  export async function deleteUsers(id) {
    return request(`/users/${id}`, {
      method: 'DELETE',
    });
  }
